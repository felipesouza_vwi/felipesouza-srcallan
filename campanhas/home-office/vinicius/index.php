<!doctype html>
<html lang="pt-br">
<head>
    <?php
    include('head.php');
    ?>
</head>
<body>
<?php
include('nav.php');
?>
<div class="body">
    <div class="container-fluid">
        <div class="container">
            <section class="col-md-12">
                <div class="row">
                    <div class="col-md-6 border-right">
                        <img class="img-primary-section"
                             src="https://www.codefest.co.uk/demo/codelander/assets/images/header.png" alt="">
                    </div>
                    <div class="col-md-6 m-auto">
                        <p class="header-subtitle">Multipurpose landing template</p>
                        <h1 class="header-title">Beautifully simple, code.</h1>
                        <p class="header-title-text">Codelander is a beautifully simple, clean and lightweight landing page template for all types
                            of businesses, with bold and bright colours.
                        </p>
                        <a href="#" class="learn-more-btn">Learn More</a>
                    </div>
                </div>
            </section>
            <!--<section class="d-flex justify-content-center">teste</section>
            <section class="d-flex justify-content-center">teste</section>
            <section class="d-flex justify-content-center">teste</section>
            <section class="d-flex justify-content-center">teste</section>
            <section class="d-flex justify-content-center">teste</section>-->
        </div>
    </div>
</div>
<?php
include('rodape.php');
?>
</body>
</html>